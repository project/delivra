
Delivra module for Drupal 7.x.
This module adds web service functions for Delivra

REQUIREMENTS
------------
* Delivra account
* Delivra API credentials
* PHP SOAP client
* Read Delivra's API Documentation here: http://www.delivra.com/api-integration/

INSTALLATION INSTRUCTIONS
-------------------------
1.  Copy the files included in the tarball into a directory named "delivra" in
    your Drupal sites/all/modules/ directory.
2.  Login as site administrator.
3.  Enable the Delivra module on the Administer -> Modules page 
    (Under the "Mail" category).
4.  Fill in required settings on the Administer -> Configuration -> 
    Web Services ->Delivra Email Service page.
5.  Enjoy.
